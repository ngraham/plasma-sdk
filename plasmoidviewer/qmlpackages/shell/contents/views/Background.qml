/*
 *   SPDX-FileCopyrightText: 2013 Antonis Tsiapaliokas <kok3rs@gmail.com>
 *
 *   SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.1
import org.kde.ksvg 1.0 as KSvg

KSvg.FrameSvgItem {
    id: bg
    imagePath: "widgets/background"
    smooth: true
}
