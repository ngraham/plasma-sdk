# Malayalam translations for plasma-sdk package.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the plasma-sdk package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-sdk\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-17 02:07+0000\n"
"PO-Revision-Date: 2019-12-12 22:28+0000\n"
"Last-Translator: Vivek KJ Pazhedath <vivekkj2004@gmail.com>\n"
"Language-Team: Swathanthra|സ്വതന്ത്ര Malayalam|മലയാളം Computing|കമ്പ്യൂട്ടിങ്ങ് <smc."
"org.in>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "shijualexonline@gmail.com,snalledam@dataone.in,vivekkj2004@gmail.com"

#: package/contents/ui/ColorEditor.qml:38
#, kde-format
msgid "Edit Colors"
msgstr ""

#: package/contents/ui/ColorEditor.qml:67
#, kde-format
msgid "Select Color"
msgstr ""

#: package/contents/ui/ColorEditor.qml:109
#, kde-format
msgid "Normal text"
msgstr ""

#: package/contents/ui/ColorEditor.qml:115
#, kde-format
msgid "Link"
msgstr ""

#: package/contents/ui/ColorEditor.qml:119
#, kde-format
msgid "Visited Link"
msgstr ""

#: package/contents/ui/ColorEditor.qml:154
#, kde-format
msgid "Complementary colors area:"
msgstr ""

#: package/contents/ui/ColorEditor.qml:169
#, kde-format
msgid "Label"
msgstr ""

#: package/contents/ui/ColorEditor.qml:192
#, kde-format
msgid "Text color:"
msgstr ""

#: package/contents/ui/ColorEditor.qml:199
#, kde-format
msgid "Background color:"
msgstr ""

#: package/contents/ui/ColorEditor.qml:206
#, kde-format
msgid "Highlight color:"
msgstr ""

#: package/contents/ui/ColorEditor.qml:213
#, kde-format
msgid "Link color:"
msgstr ""

#: package/contents/ui/ColorEditor.qml:220
#, kde-format
msgid "Visited link color:"
msgstr ""

#: package/contents/ui/ColorEditor.qml:228
#, kde-format
msgid "Button text color:"
msgstr ""

#: package/contents/ui/ColorEditor.qml:235
#, kde-format
msgid "Button background color:"
msgstr ""

#: package/contents/ui/ColorEditor.qml:242
#, kde-format
msgid "Button mouse over color:"
msgstr ""

#: package/contents/ui/ColorEditor.qml:249
#, kde-format
msgid "Button focus color:"
msgstr ""

#: package/contents/ui/ColorEditor.qml:257
#, kde-format
msgid "Text view text color:"
msgstr ""

#: package/contents/ui/ColorEditor.qml:264
#, kde-format
msgid "Text view background color:"
msgstr ""

#: package/contents/ui/ColorEditor.qml:271
#, kde-format
msgid "Text view mouse over color:"
msgstr ""

#: package/contents/ui/ColorEditor.qml:278
#, kde-format
msgid "Text view focus color:"
msgstr ""

#: package/contents/ui/ColorEditor.qml:286
#, kde-format
msgid "Complementary text color:"
msgstr ""

#: package/contents/ui/ColorEditor.qml:293
#, kde-format
msgid "Complementary background color:"
msgstr ""

#: package/contents/ui/ColorEditor.qml:300
#, kde-format
msgid "Complementary mouse over color:"
msgstr ""

#: package/contents/ui/ColorEditor.qml:307
#, kde-format
msgid "Complementary focus color:"
msgstr ""

#: package/contents/ui/ColorEditor.qml:322
#: package/contents/ui/MetadataEditor.qml:136
#, kde-format
msgid "OK"
msgstr ""

#: package/contents/ui/ColorEditor.qml:327
#: package/contents/ui/MetadataEditor.qml:142
#, kde-format
msgid "Cancel"
msgstr ""

#: package/contents/ui/delegates/actionbutton.qml:34
#: package/contents/ui/delegates/actionbutton.qml:38
#: package/contents/ui/delegates/checkmarks.qml:30
#: package/contents/ui/delegates/checkmarks.qml:34
#: package/contents/ui/delegates/checkmarks.qml:38
#: package/contents/ui/delegates/checkmarks.qml:42
#, kde-format
msgid "Option"
msgstr ""

#: package/contents/ui/delegates/button.qml:26
#, kde-format
msgid "ToolButton"
msgstr ""

#: package/contents/ui/delegates/button.qml:30
#: package/contents/ui/fakecontrols/Button.qml:22
#, kde-format
msgid "Button"
msgstr ""

#: package/contents/ui/delegates/tabbar.qml:28
#, kde-format
msgid "Tab1"
msgstr ""

#: package/contents/ui/delegates/tabbar.qml:31
#, kde-format
msgid "Tab2"
msgstr ""

#: package/contents/ui/delegates/tabbar.qml:34
#, kde-format
msgid "Tab3"
msgstr ""

#: package/contents/ui/delegates/textfield.qml:27
#, kde-format
msgid "Text"
msgstr ""

#: package/contents/ui/fakecontrols/CheckBox.qml:73
#, kde-format
msgid "Checkbox"
msgstr ""

#: package/contents/ui/fakecontrols/LineEdit.qml:33
#, kde-format
msgid "Text input…"
msgstr ""

#: package/contents/ui/main.qml:30
#, kde-format
msgid "New Theme…"
msgstr ""

#: package/contents/ui/main.qml:46
#, kde-format
msgid "Theme:"
msgstr ""

#: package/contents/ui/main.qml:58
#, kde-format
msgid "Open Folder"
msgstr ""

#: package/contents/ui/main.qml:63
#, kde-format
msgid "Edit Metadata…"
msgstr ""

#: package/contents/ui/main.qml:80
#, kde-format
msgid "Edit Colors…"
msgstr ""

#: package/contents/ui/main.qml:96
#, kde-format
msgid "Help"
msgstr ""

#: package/contents/ui/main.qml:101
#, fuzzy, kde-format
#| msgid "Search..."
msgid "Search…"
msgstr "തിരയുക..."

#: package/contents/ui/main.qml:219
#, kde-format
msgid "This is a readonly, system wide installed theme"
msgstr ""

#: package/contents/ui/main.qml:224
#, kde-format
msgid "Preview:"
msgstr ""

#: package/contents/ui/main.qml:239
#, kde-format
msgid "Image path: %1"
msgstr ""

#: package/contents/ui/main.qml:244
#, kde-format
msgid "Description: %1"
msgstr ""

#: package/contents/ui/main.qml:249
#, kde-format
msgid "Missing from this theme"
msgstr ""

#: package/contents/ui/main.qml:249
#, kde-format
msgid "Present in this theme"
msgstr ""

#: package/contents/ui/main.qml:254
#, kde-format
msgid "Show Margins"
msgstr ""

#: package/contents/ui/main.qml:257
#, kde-format
msgid "Create with Editor…"
msgstr ""

#: package/contents/ui/main.qml:257
#, kde-format
msgid "Open In Editor…"
msgstr ""

#: package/contents/ui/MetadataEditor.qml:23
#, kde-format
msgid "New Theme"
msgstr ""

#: package/contents/ui/MetadataEditor.qml:23
#, kde-format
msgid "Edit Theme"
msgstr ""

#: package/contents/ui/MetadataEditor.qml:58
#, kde-format
msgid "Warning: don't change author or license for themes you don't own"
msgstr ""

#: package/contents/ui/MetadataEditor.qml:69
#, kde-format
msgid "Theme Name:"
msgstr ""

#: package/contents/ui/MetadataEditor.qml:85
#, kde-format
msgid "This theme name already exists"
msgstr ""

#: package/contents/ui/MetadataEditor.qml:94
#, kde-format
msgid "Author:"
msgstr ""

#: package/contents/ui/MetadataEditor.qml:102
#, kde-format
msgid "Email:"
msgstr ""

#: package/contents/ui/MetadataEditor.qml:110
#, kde-format
msgid "License:"
msgstr ""

#: package/contents/ui/MetadataEditor.qml:121
#, kde-format
msgid "Website:"
msgstr ""

#: src/main.cpp:37
#, kde-format
msgid "Plasma Theme Explorer"
msgstr ""

#: src/main.cpp:41
#, kde-format
msgid "The theme to open"
msgstr ""
